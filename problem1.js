const object = require("../JsDrillFiles/users.cjs"); //importing object
function intrestedInGames(object) {
  //creating function
  let keys = Object.keys(object); //stored key in array
  keys.forEach((value) => {
    //iterating inside array using forEach method
    if (
      object[value].hasOwnProperty("interests") && //interest property present or not
      object[value]["interests"][0].includes("Video Games") //checking video Games present or not in string using includes method
    ) {
      console.log(value);
    }
  });
}
intrestedInGames(object); //calling the function
