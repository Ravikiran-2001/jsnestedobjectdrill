const object = require("../JsDrillFiles/users.cjs"); //importing object
function usersInGermany(object) {
  //creating function
  let keys = Object.keys(object); //stored key in array
  keys.forEach((value) => {
    //iterating inside array using forEach method
    if (
      object[value].hasOwnProperty("interests") && //interest property present or not
      object[value]["nationality"] === "Germany" //checking Germany present or not
    ) {
      console.log(value);
    }
  });
}
usersInGermany(object); //calling function
