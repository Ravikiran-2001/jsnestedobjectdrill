const object = require("../JsDrillFiles/users.cjs"); //importing object
const sortedByDesignation = Object.keys(object).sort((userName1, userName2) => {
  //extracted keys and sorted its designation using sort function
  const user1 = object[userName1]; //details of userName1
  const user2 = object[userName2]; //details of userName2
  const designationLevel = {
    //created object and ranked all designation
    "Senior Golang Developer": 3,
    "Senior Javascript Developer": 3,
    "Python Developer": 2,
    "Intern - Javascript": 1,
    "Intern - Golang": 1,
  };
  const SeniorLevel1 = designationLevel[user1.desgination] || 0; //assigning ranks
  const SeniorLevel2 = designationLevel[user2.desgination] || 0; //assigning ranks

  if (SeniorLevel1 !== SeniorLevel2) {
    //if both ranks are different it will return true
    return SeniorLevel2 - SeniorLevel1; //if -ve value is returned second argument added second and than first argument and vice-versa
  }
  return user2.age - user1.age; //if both ranks are same it will compare with age
});
console.log(sortedByDesignation); //printing output
