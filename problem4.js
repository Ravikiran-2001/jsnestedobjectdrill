const object = require("../JsDrillFiles/users.cjs"); //importing object
function mastersDegree(object) {
  //creating function
  let keys = Object.keys(object); //stored key in array
  keys.forEach((value) => {
    //iterating inside array using forEach method
    if (
      object[value].hasOwnProperty("interests") && //interest property present or not
      object[value]["qualification"] === "Masters" //checking Masters present or not
    ) {
      console.log(value);
    }
  });
}
mastersDegree(object); //calling function
