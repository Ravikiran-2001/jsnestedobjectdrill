const object = require("../JsDrillFiles/users.cjs"); //importing object
function groupBasedOnProgramming(object) {
  //creating function
  const group = {}; //empty object
  let keys = Object.keys(object); //stored key in array

  keys.forEach((value) => {
    //iterating inside array using forEach method
    const user = object[value];
    const language = getLanguage(user.desgination); //calling function
    if (!group.hasOwnProperty(language)) {
      //if language does'nt exist we create a property and assign empty array
      group[language] = [];
    }
    group[language].push(value); //will push language inside array
  });
  return group;
}

function getLanguage(desgination) {
  const language = ["Golang", "Javascript", "Python"]; //created array which includes all language
  let programmingLanguage;
  language.forEach((value) => {
    if (desgination.toLowerCase().includes(value.toLowerCase())) {
      //checking language includes in string or not
      programmingLanguage = value;
      return;
    }
  });
  return programmingLanguage;
}
console.log(groupBasedOnProgramming(object)); //calling and printing returned output
